import java.util.ArrayList;

public class MatrixIterator<ArrayList> {
    private int row = 0;       //строка текущего элемента
    private int col = 0;       //столбец текущего элемента
    private ArrayList[][] matrix;

    public MatrixIterator(ArrayList[][] matrix) {
        this.matrix = matrix;
    }

    public boolean hasNext() {
        if (row + 1 == matrix.length) {
            return col < matrix[row].length;
        }
        return row < matrix.length;
    }

    public ArrayList next() {
        if (col == matrix[row].length) {
            col = 0;
            row++;
        }
        return  matrix[row][col++];
    }


    public void remove() {
        matrix.remove[row][col-1];
        col--;
    }
}
