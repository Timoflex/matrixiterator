import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {
        ArrayList[][] matrix = new ArrayList[3][2];
        matrix[0][0] = new ArrayList (1);
        matrix[0][1] = new ArrayList (2);
        matrix[1][0] = new ArrayList (4);
        matrix[1][1] = new ArrayList (5);
        MatrixIterator<ArrayList> iterator = new MatrixIterator<ArrayList>(matrix);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
